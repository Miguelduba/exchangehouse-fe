import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ExchangeComponent} from './exchange.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RequestService} from '../service/request.service';
import {MatFormFieldModule, MatGridListModule, MatInputModule, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {ExchangeRsDto} from '../model';
import {of, throwError} from 'rxjs';
import {HttpClientModule, HttpErrorResponse} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('ExchangeComponent', () => {
  let component: ExchangeComponent;
  let fixture: ComponentFixture<ExchangeComponent>;
  let requestService: RequestService;
  let snackbar: MatSnackBar;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatGridListModule,
        HttpClientModule,
        MatSnackBarModule,
        MatInputModule,
        BrowserAnimationsModule],
      declarations: [ ExchangeComponent ],
      providers: [FormBuilder, RequestService, MatSnackBar],
    }).compileComponents();
    fixture = TestBed.createComponent(ExchangeComponent);
    component = fixture.componentInstance;
    requestService = TestBed.get(RequestService);
    snackbar = TestBed.get(MatSnackBar);
  }));

  it('Should send form', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const response: ExchangeRsDto = {base: 'USD', change: 'ERU', exchangeValue: 0.90};
      const base = component.from.get('base');
      base.setValue(1000);
      spyOn(requestService, 'get').and.returnValue(of(response));
      component.sendExchange();
      expect(component.from.get('change').value).toEqual(component.from.get('base').value * response.exchangeValue);
    });
  });

  it('Should show error message', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const base = component.from.get('base');
      base.setValue(1000);
      const error: HttpErrorResponse = new HttpErrorResponse({});
      spyOn(requestService, 'post').and.returnValue(throwError({status: 500}));
      const snackbarspy = spyOn(snackbar, 'open').and.callThrough();
      component.sendExchange();
      expect(snackbarspy).toHaveBeenCalled();
    });
  });
});
