import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RequestService} from '../service/request.service';
import {environment} from '../../environments/environment';
import {ExchangeRsDto} from '../model';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.scss']
})
export class ExchangeComponent implements OnInit {

  from: FormGroup;
  exchangeId = 1;

  constructor(private builder: FormBuilder,
              private sendRequest: RequestService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.from = this.builder.group({
      base: ['', [Validators.required]],
      change: ['']
    });
    this.from.get('change').disable();
  }

  sendExchange() {
    if (!this.from.invalid) {
      this.sendRequest.get(environment.endpoints.getExchange + '/' + this.exchangeId).subscribe(
        (data: ExchangeRsDto) => {
            this.calculateExchange(data.exchangeValue);
        },
        (error) => {
          this.snackBar.open('Upppss Sorry we have some problem', '',  {duration: 3000});
          console.log(error);
        }
      );
    }
  }

  setChangeValue(changeValue: number): void {
    this.from.get('change').setValue(changeValue);
  }
  calculateExchange(exchangeValue: number): void {
    const total = this.from.get('base').value * exchangeValue;
    this.setChangeValue(total);
  }
}
