import {TestBed} from '@angular/core/testing';

import {RequestService} from './request.service';
import {ExchangeRqDto, ExchangeRsDto} from '../model';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('RequestService', () => {
  let service: RequestService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RequestService]
    });
    service = TestBed.get(RequestService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Call Service', () => {
    const responseBody: ExchangeRsDto = {
      base: 'USD',
      change: 'EUR',
      exchangeValue: 0.9,
    };
    const requestBody: ExchangeRqDto = {
      base: 1000,
      type: 1
    };
    service.post('/post', requestBody).subscribe((resp: ExchangeRsDto) => {
      expect(resp).toEqual(responseBody);
    });
    const request = httpMock.expectOne(`${service.server}/post`);
    expect(request.request.method).toBe('POST');
    request.flush(responseBody);
  });
});
