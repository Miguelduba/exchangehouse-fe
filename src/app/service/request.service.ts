import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';
import {ResponseCache} from '../model/responseCache';
import {ExchangeRsDto} from '../model';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  responseCache: ResponseCache[];
  server: string;
  tenMinutes: number;

  constructor(private http: HttpClient) {
    this.server = environment.protocol + environment.domain + environment.port;
    this.responseCache = [];
    this.tenMinutes = 600000;
  }

  public post(url: string, body: any): Observable<any> {
    return this.http.post(this.server + url, body);
  }

  public get(url: string): Observable<any> {
    const responseIndex = this.responseCache
      .findIndex(item => item.url === url);
    if (responseIndex > -1) {
      if ((Date.now().valueOf() - this.responseCache[responseIndex].lastCall.valueOf()) < this.tenMinutes) {
        return of(this.responseCache[responseIndex].response);
      }
      this.responseCache.splice(responseIndex, 1);
    }
    const newResponse = this.http.get(this.server + url);
    newResponse.subscribe( (data: ExchangeRsDto) => this.responseCache.push(new ResponseCache(url, data, new Date())));
    return newResponse;
  }
}

