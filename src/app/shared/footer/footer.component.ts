import {Component, OnInit} from '@angular/core';
import {InfoFooter} from 'src/app/model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  infosFooter: InfoFooter [] = [];

  constructor() {
    this.infosFooter.push(new InfoFooter('Test1', 'A big Description'));
    this.infosFooter.push(new InfoFooter('Test2', 'A big Description'));
    this.infosFooter.push(new InfoFooter('Test3', 'A big Description'));
  }

  ngOnInit() {
  }

}
