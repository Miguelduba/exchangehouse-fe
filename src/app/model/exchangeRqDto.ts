export class ExchangeRqDto {

  constructor(base: number, type: number) {
    this.base = base;
    this.type = type;
  }

  base: number;
  type: number;
}
