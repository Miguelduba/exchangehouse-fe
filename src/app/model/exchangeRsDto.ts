export class ExchangeRsDto {
  base: string;
  change: string;
  exchangeValue: number;
}
