import {ExchangeRsDto} from './exchangeRsDto';

export class ResponseCache {

  constructor(url: string, response: ExchangeRsDto, lastCall: Date) {
    this.url = url;
    this.response = response;
    this.lastCall = lastCall;
  }

  url: string;
  response: ExchangeRsDto;
  lastCall: Date;
}
