import {Component, HostBinding, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  themingSubscription: Subscription;

  constructor() { }

  @HostBinding('class') public cssClass;

  ngOnInit() {
  }

  ngOnDestroy() {
    this.themingSubscription.unsubscribe();
  }
}
